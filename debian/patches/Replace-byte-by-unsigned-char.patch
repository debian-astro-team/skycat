From: Ole Streicher <olebole@debian.org>
Date: Mon, 30 May 2022 23:17:58 +0200
Subject: Replace byte by unsigned char

byte is already defined as std:byte
---
 rtd/generic/ByteImageData.C     |  2 +-
 rtd/generic/ByteImageData.h     | 22 +++++++++++-----------
 rtd/generic/DoubleImageData.h   |  2 +-
 rtd/generic/FloatImageData.h    |  2 +-
 rtd/generic/ImageData.h         |  3 +--
 rtd/generic/ImageTemplates.icc  | 18 +++++++++---------
 rtd/generic/LongImageData.h     |  2 +-
 rtd/generic/LongLongImageData.h |  2 +-
 rtd/generic/LookupTable.h       |  2 --
 rtd/generic/ShortImageData.h    |  2 +-
 rtd/generic/UShortImageData.h   |  2 +-
 rtd/generic/XImageData.C        |  4 ++--
 rtd/generic/XImageData.h        | 24 ++++++++++++------------
 13 files changed, 42 insertions(+), 45 deletions(-)

diff --git a/rtd/generic/ByteImageData.C b/rtd/generic/ByteImageData.C
index 0384739..90d0395 100644
--- a/rtd/generic/ByteImageData.C
+++ b/rtd/generic/ByteImageData.C
@@ -64,7 +64,7 @@ int ByteImageData::parseBlank(const char* value) {
  * except that they work on a different raw data type
  */
 #define CLASS_NAME ByteImageData
-#define DATA_TYPE byte
+#define DATA_TYPE unsigned char
 #define NTOH(x) (x)
 #include "ImageTemplates.icc"
 #undef CLASS_NAME
diff --git a/rtd/generic/ByteImageData.h b/rtd/generic/ByteImageData.h
index cb54bbc..6dcbe3c 100644
--- a/rtd/generic/ByteImageData.h
+++ b/rtd/generic/ByteImageData.h
@@ -33,31 +33,31 @@ private:
     long blank_;
 
     // get value as unsigned short
-    inline ushort convertToUshort(byte b) {
+    inline ushort convertToUshort(unsigned char b) {
 	return (ushort)b;
     }
 
     // return X image pixel value for raw image value
-    inline byte lookup(byte b) {
+    inline unsigned char lookup(unsigned char b) {
 	if ( !haveBlank_ ) return lookup_[(ushort)b];
 	if ( b != blank_ ) return lookup_[(ushort)b];
 	return lookup_[128];
     } 
-    inline unsigned long llookup(byte b) {
+    inline unsigned long llookup(unsigned char b) {
 	if ( !haveBlank_ ) return lookup_[(ushort)b];
 	if ( b != blank_ ) return lookup_[(ushort)b];
 	return lookup_[128];
     }
 
     // return NTOH converted value evtl. subtracted with corresponding bias value
-    byte getVal(byte* p, int idx);
-
-    int getXsamples(byte *rawImage, int idx, int wbox, byte *samples);
-    int getBsamples(byte *rawImage, int idx, int wbox, byte *samples);
-    int getCsamples(byte *rawImage, int idx, int wbox, byte *samples);
-    byte getMedian(byte *samples, int n);
-    byte getBoxVal(byte *rawImage, int idx, int wbox, byte *samples, int xs);
-    byte getRMS(byte *samples, int n);
+    unsigned char getVal(unsigned char* p, int idx);
+
+    int getXsamples(unsigned char *rawImage, int idx, int wbox, unsigned char *samples);
+    int getBsamples(unsigned char *rawImage, int idx, int wbox, unsigned char *samples);
+    int getCsamples(unsigned char *rawImage, int idx, int wbox, unsigned char *samples);
+    unsigned char getMedian(unsigned char *samples, int n);
+    unsigned char getBoxVal(unsigned char *rawImage, int idx, int wbox, unsigned char *samples, int xs);
+    unsigned char getRMS(unsigned char *samples, int n);
 
 protected:
     // convert cut values to short range
diff --git a/rtd/generic/DoubleImageData.h b/rtd/generic/DoubleImageData.h
index 293b8c3..e784ddc 100644
--- a/rtd/generic/DoubleImageData.h
+++ b/rtd/generic/DoubleImageData.h
@@ -40,7 +40,7 @@ private:
     // Convert the given double image value to byte, scaling to short
     // first and then using the short value as an index in the color
     // lookup table.
-    inline byte lookup( double f ) {
+    inline unsigned char lookup( double f ) {
         return lookup_[(ushort)scaleToShort(f)];
     }
     inline unsigned long llookup( double f ) {
diff --git a/rtd/generic/FloatImageData.h b/rtd/generic/FloatImageData.h
index e9af540..761d56b 100644
--- a/rtd/generic/FloatImageData.h
+++ b/rtd/generic/FloatImageData.h
@@ -44,7 +44,7 @@ private:
     // Convert the given float image value to byte, scaling to short
     // first and then using the short value as an index in the color
     // lookup table.
-    inline byte lookup(float f) {return lookup_[(ushort)scaleToShort(f)];}
+    inline unsigned char lookup(float f) {return lookup_[(ushort)scaleToShort(f)];}
     inline unsigned long llookup(float f) {return lookup_[(ushort)scaleToShort(f)];}
 
     // return NTOH converted value evtl. subtracted with corresponding bias value
diff --git a/rtd/generic/ImageData.h b/rtd/generic/ImageData.h
index e66552a..d2b34cc 100644
--- a/rtd/generic/ImageData.h
+++ b/rtd/generic/ImageData.h
@@ -54,7 +54,6 @@
 #define isnan(x) ((x) != (x))
 #endif
 
-typedef unsigned char byte;	// type of XImage data (no longer ...)
 struct ImageDataParams;		// forward ref
 struct ImageDataHistogram;      // forward ref
 
@@ -111,7 +110,7 @@ protected:
 
     // pointers to the caller's XImage and data, which this class writes to
     ImageDisplay* xImage_;
-    byte* xImageData_;
+    unsigned char* xImageData_;
 
     // this represents the contents of the image file or other source
     // (uses reference counting so we can share this with other views)
diff --git a/rtd/generic/ImageTemplates.icc b/rtd/generic/ImageTemplates.icc
index 4f69a62..d034aed 100644
--- a/rtd/generic/ImageTemplates.icc
+++ b/rtd/generic/ImageTemplates.icc
@@ -110,7 +110,7 @@ inline DATA_TYPE CLASS_NAME::getVal(DATA_TYPE* p, int idx)
 	switch (bias->type) {
 	case BYTE_IMAGE:
 	case X_IMAGE:
-	    return (DATA_TYPE)NTOH(*(p + idx)) - (DATA_TYPE)((byte)(*((byte *)bias->ptr     + biasIdx)));
+	    return (DATA_TYPE)NTOH(*(p + idx)) - (DATA_TYPE)((unsigned char)(*((unsigned char *)bias->ptr     + biasIdx)));
 	case USHORT_IMAGE:
 	    return (DATA_TYPE)NTOH(*(p + idx)) - (DATA_TYPE)((ushort)(*((ushort *)bias->ptr + biasIdx)));
 	case SHORT_IMAGE:
@@ -143,7 +143,7 @@ inline DATA_TYPE CLASS_NAME::getVal(DATA_TYPE* p, int idx)
 	switch (bias->type) {
 	case BYTE_IMAGE:
 	case X_IMAGE:
-	    return (DATA_TYPE)NTOH(*(p + idx)) - (DATA_TYPE)((byte)(*((byte *)bias->ptr             + biasIdx)));
+	    return (DATA_TYPE)NTOH(*(p + idx)) - (DATA_TYPE)((unsigned char)(*((unsigned char *)bias->ptr             + biasIdx)));
 	case USHORT_IMAGE:
 	    return (DATA_TYPE)NTOH(*(p + idx)) - (DATA_TYPE)((ushort)SWAP16(*((ushort *)bias->ptr   + biasIdx)));
 	case SHORT_IMAGE:
@@ -544,7 +544,7 @@ void CLASS_NAME::rawToXImage(int x0, int y0, int x1, int y1,
     // source/dest images
     register DATA_TYPE* rawImage = (DATA_TYPE*)image_.dataPtr();
     register int src;
-    register byte* dest = xImageData_;
+    register unsigned char* dest = xImageData_;
 
     initGetVal();  // init flag for speeding up bias subtraction
 
@@ -637,8 +637,8 @@ void CLASS_NAME::rawToXImage(int x0, int y0, int x1, int y1,
 void CLASS_NAME::grow(int x0, int y0, int x1, int y1,
 		      int dest_x, int dest_y)
 {
-    register byte *p, *q;
-    register byte c;
+    register unsigned char *p, *q;
+    register unsigned char c;
     register int i, j, n, m;
     register int xs = xScale_, ys = yScale_;
 
@@ -649,8 +649,8 @@ void CLASS_NAME::grow(int x0, int y0, int x1, int y1,
     // source/dest images
     register DATA_TYPE* rawImage = (DATA_TYPE*)image_.dataPtr();
     register int src;
-    register byte* dest = xImageData_;
-    register byte* end = xImageData_ + xImageSize_;
+    register unsigned char* dest = xImageData_;
+    register unsigned char* end = xImageData_ + xImageSize_;
 
     initGetVal();  // init flag for speeding up bias subtraction
 
@@ -1141,8 +1141,8 @@ void CLASS_NAME::shrink(int x0, int y0, int x1, int y1, int dest_x, int dest_y)
     // source/dest images
     DATA_TYPE* rawImage = (DATA_TYPE*)image_.dataPtr();
     int src;
-    byte* dest = xImageData_;
-    byte* end = xImageData_ + xImageSize_ - 1;
+    unsigned char* dest = xImageData_;
+    unsigned char* end = xImageData_ + xImageSize_ - 1;
 
     DATA_TYPE maxval = 0;
 
diff --git a/rtd/generic/LongImageData.h b/rtd/generic/LongImageData.h
index 6eef1b9..2169368 100644
--- a/rtd/generic/LongImageData.h
+++ b/rtd/generic/LongImageData.h
@@ -51,7 +51,7 @@ private:
     }
 
     // return X image pixel value for raw image value
-    inline byte lookup(FITS_LONG l) {
+    inline unsigned char lookup(FITS_LONG l) {
 	return lookup_[convertToUshort(l)];
     }
     inline unsigned long llookup(FITS_LONG l) {
diff --git a/rtd/generic/LongLongImageData.h b/rtd/generic/LongLongImageData.h
index 54beccb..38375c4 100644
--- a/rtd/generic/LongLongImageData.h
+++ b/rtd/generic/LongLongImageData.h
@@ -41,7 +41,7 @@ private:
     }
 
     // return X image pixel value for raw image value
-    inline byte lookup(FITS_LONGLONG l) {
+    inline unsigned char lookup(FITS_LONGLONG l) {
 	return lookup_[convertToUshort(l)];
     }
     inline unsigned long llookup(FITS_LONGLONG l) {
diff --git a/rtd/generic/LookupTable.h b/rtd/generic/LookupTable.h
index 89fd725..52fcbc8 100644
--- a/rtd/generic/LookupTable.h
+++ b/rtd/generic/LookupTable.h
@@ -22,8 +22,6 @@
 #include <cstring>
 #include <cstdlib>
 
-typedef unsigned char byte;
-
 
 /* 
  * This class is used internally for reference counting and subclassing.
diff --git a/rtd/generic/ShortImageData.h b/rtd/generic/ShortImageData.h
index 95c5a48..8122e5b 100644
--- a/rtd/generic/ShortImageData.h
+++ b/rtd/generic/ShortImageData.h
@@ -45,7 +45,7 @@ private:
     }
 
     // return X image pixel value for raw image value
-    byte lookup(short s) {
+    unsigned char lookup(short s) {
         return lookup_[convertToUshort(s)];
     }
     unsigned long llookup(short s) {
diff --git a/rtd/generic/UShortImageData.h b/rtd/generic/UShortImageData.h
index 6bc424a..5d41374 100644
--- a/rtd/generic/UShortImageData.h
+++ b/rtd/generic/UShortImageData.h
@@ -37,7 +37,7 @@ private:
     ushort convertToUshort(int l);
 
     // return X image pixel value for raw image value
-    inline byte lookup(ushort s) {
+    inline unsigned char lookup(ushort s) {
         return lookup_[convertToUshort(s)];
     }
     inline unsigned long llookup(ushort s) {
diff --git a/rtd/generic/XImageData.C b/rtd/generic/XImageData.C
index 2e4f4a3..15b92c5 100644
--- a/rtd/generic/XImageData.C
+++ b/rtd/generic/XImageData.C
@@ -37,7 +37,7 @@ int XImageData::parseBlank(const char* value) {
     long l;
     int n = sscanf(value, "%ld", &l);
     if ( n > 0 ) {
-        blank_ = (byte) l;
+        blank_ = (unsigned char) l;
     }
     return n;
 }
@@ -48,7 +48,7 @@ int XImageData::parseBlank(const char* value) {
  * except that they work on a different raw data type
  */
 #define CLASS_NAME XImageData
-#define DATA_TYPE byte
+#define DATA_TYPE unsigned char
 #define NTOH(x) (x)
 #include "ImageTemplates.icc"
 #undef CLASS_NAME
diff --git a/rtd/generic/XImageData.h b/rtd/generic/XImageData.h
index 92e7ab0..f51b759 100644
--- a/rtd/generic/XImageData.h
+++ b/rtd/generic/XImageData.h
@@ -29,35 +29,35 @@
 class XImageData : public ImageData {
 private:
     // value of blank pixel, if known (if haveBlankPixel_ is nonzero)
-    byte blank_;
+    unsigned char blank_;
 
     // get value as unsigned short
-    inline ushort convertToUshort(byte b) {
+    inline ushort convertToUshort(unsigned char b) {
 	return (ushort)b;
     }
 
 
     // return X image pixel value for raw image value
-    inline byte lookup(byte b) {
+    inline unsigned char lookup(unsigned char b) {
 	if ( !haveBlank_ ) return b;
 	if ( b != blank_ ) return b;
 	return blank_;
     } 
-    inline unsigned long llookup(byte b) {
+    inline unsigned long llookup(unsigned char b) {
 	if ( !haveBlank_ ) return b;
 	if ( b != blank_ ) return b;
 	return blank_;
     }
 
     // return NTOH converted value evtl. subtracted with corresponding bias value
-    byte getVal(byte* p, int idx);
-
-    int getXsamples(byte *rawImage, int idx, int wbox, byte *samples);
-    int getBsamples(byte *rawImage, int idx, int wbox, byte *samples);
-    int getCsamples(byte *rawImage, int idx, int wbox, byte *samples);
-    byte getMedian(byte *samples, int n);
-    byte getBoxVal(byte *rawImage, int idx, int wbox, byte *samples, int xs);
-    byte getRMS(byte *samples, int n);
+    unsigned char getVal(unsigned char* p, int idx);
+
+    int getXsamples(unsigned char *rawImage, int idx, int wbox, unsigned char *samples);
+    int getBsamples(unsigned char *rawImage, int idx, int wbox, unsigned char *samples);
+    int getCsamples(unsigned char *rawImage, int idx, int wbox, unsigned char *samples);
+    unsigned char getMedian(unsigned char *samples, int n);
+    unsigned char getBoxVal(unsigned char *rawImage, int idx, int wbox, unsigned char *samples, int xs);
+    unsigned char getRMS(unsigned char *samples, int n);
 
 protected:
     // no conversion necessary
