Source: skycat
Maintainer: Debian Astronomy Maintainers <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>,
           Phil Wyett <philip.wyett@kathenas.org>
Section: science
Priority: optional
Build-Depends: blt-dev,
               debhelper-compat (= 13),
               iwidgets4,
               libcfitsio-dev,
               libwcstools-dev,
               tcl8.6-dev,
               tk8.6-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/skycat
Vcs-Git: https://salsa.debian.org/debian-astro-team/skycat.git
Homepage: https://www.eso.org/sci/observing/tools/skycat.html

Package: skycat
Architecture: any
Depends: blt,
         iwidgets4,
         libtk-img,
         tcl-expect,
         tcl8.6,
         tk8.6,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Image visualization and access to catalogs and data for astronomy
 The ESO/Starlink Skycat tool combines the image display capabilities
 of the RTD (Real-Time Display) with a set of classes for accessing
 astronomical catalogs locally and over the network using HTTP. The tool
 allows you to view FITS images from files or from the Digitized Sky
 Survey (DSS).
